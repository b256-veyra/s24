// console.log("Hello World");

function getCube(number) {
  return number ** 3;
}

let num = 2;
let cube = getCube(num);
console.log(`The cube of ${num} is ${cube}.`);

// -------

let addressArr = ['258', 'Washington Ave NW', 'California', '90011'];

let addressArr1 = addressArr[0];
let addressArr2 = addressArr[1];
let addressArr3 = addressArr[2];
let addressArr4 = addressArr[3];
let addressArr5 = addressArr[4];

let completeAddress = `I live at ${addressArr1} ${addressArr2}, ${addressArr3} ${addressArr4}`;

console.log(completeAddress);

const crocodile = {
  name: "Lolong",
  species: "saltwater crocodile",
  weight: "1075",
  measurement: "20 ft 3 in"
}

const {name, species, weight, measurement} = crocodile;

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);


let numbersArr = [1, 3, 5, 7];

numbersArr.forEach((numbersArr) => console.log(numbersArr));

let reducedNumber = numbersArr.reduce((x, y) => {
 return x + y;
});
console.log(reducedNumber);

class dog {
  constructor(name, age, breed){
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

let dog1 = new dog("Tally", 4, "Belgian Malinois");
console.log(dog1);
let dog2 = new dog("Hachiko", 10, "Shiba Inu");
console.log(dog2);
let dog3 = new dog("Bruno", 3, "Shih Tzu");
console.log(dog3);




// unfinished


























































































